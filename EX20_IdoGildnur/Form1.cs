﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX20_IdoGildnur
{
    public partial class front_interface : Form
    {

        NetworkStream clientStream;
        TcpClient client = new TcpClient();
        string myMessage = "";
        string opponentMessage = "";
        int numOfSendedCards = 0; // 0 - niether one of the players sent a card; 1 - this player sent a card; 2 - both players sent cards;
        PictureBox[] myCards = new PictureBox[10];

        public front_interface()
        {
            InitializeComponent();
            Process.Start("Server.pyc");
            DisableButtons();
            ConnectToGameServer();
            Thread myThread = new Thread(GetMessageFromServer);
            myThread.Start();
        }

        public void DisableButtons()
        {
            foreach (Control c in Controls) //Passes on every button and controller
            {
                Button b = c as Button;
                if (b != null)
                    if (b.Enabled == true)
                        b.Enabled = false; //Disable
            }
        }

        public void EnableButtons()
        {
            foreach (Control c in Controls) //Passes on every button and controller
            {
                Button b = c as Button;
                if (b != null)
                    if (b.Enabled == false)
                        b.Enabled = true; //Enable
            }
        }

        private void front_interface_FormClosed(object sender, FormClosedEventArgs e)
        {
            MessageBox.Show("your score: " + myScore.Text + "\n\nOpponen't score: " + opponentScore.Text);
            SendMessageToServer(CreateMessageToServer("2"));
        }

        private void forfeitBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("your score: " + myScore.Text + "\n\nOpponen't score: " + opponentScore.Text);
            SendMessageToServer(CreateMessageToServer("2"));
            Close();
        }

        public void ConnectToGameServer()
        {
            try
            {
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                this.client.Connect(serverEndPoint);
                this.clientStream = this.client.GetStream();
            }
            catch (Exception) 
            {
                MessageBox.Show("Error while trying to connect");
                Application.Exit(); 
            }
        }

        public void SendMessageToServer(string msg)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(msg);
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
        }

        public string CreateMessageToServer(string key)
        {
            string msg = "";
            if (key == "1")
                msg = key + GetRandomNum() + GetRandomLetter();
            else
                msg = key + "000";
            return msg;
        }

        public void GetMessageFromServer(Object obj)
        {
            while (true) //client.Connected
            {
                string input = "";
                try
                {
                    byte[] bufferIn = new byte[4];
                    int bytesRead = clientStream.Read(bufferIn, 0, 4);
                    //input = new ASCIIEncoding().GetString(bufferIn, 0, bytesRead);
                    Invoke((MethodInvoker)delegate { input = new ASCIIEncoding().GetString(bufferIn, 0, bytesRead); }); //get message from server
                    if (input[0] == '0') //initialize message
                    {
                        GenerateCards(); //creating the cards
                        EnableButtons(); //enable all buttons
                    }
                    else if (input[0] == '2') //close connection and exit the game
                    {
                        MessageBox.Show("your score: " + myScore.Text + "\n\nOpponen't score: " + opponentScore.Text);
                        Close();
                    }
                    else if (input[0] == '1') //The other player sent his card.
                    {
                        numOfSendedCards++; //Appendtion
                        if (numOfSendedCards == 2) //check if you both send card
                        {
                            WinnerCalc(input); //compares who wins
                            PicturePlacer(input, opponent_card); //show the card of the opponen't
                            numOfSendedCards = 0;
                        }
                        else
                            opponentMessage = input; //save the opponen't card 
                    }
                }
                catch (Exception)
                {
                    //MessageBox.Show("Error while trying to recieve opponent's data.");
                }
            }
        }


        public void FoldAllCards()
        {
            for (int i = 0; i < 10; i++)
                this.myCards[i].Image = global::EX20_IdoGildnur.Properties.Resources.card_back_red;
        }


        public void PicturePlacer(string card, PictureBox picture)
        {
            string[] fileName;
            string num = "";
            int cardNum = Int32.Parse(card.Substring(1, 2));
            char cardSign = card[card.Length - 1];
            cardSign = char.ToLower(cardSign);

            if (cardNum >= 2 && cardNum <= 10)
                num = cardNum.ToString();
            else if (cardNum == 11)
                num = "jack";
            else if (cardNum == 12)
                num = "queen";
            else if (cardNum == 13)
                num = "king";
            else if (cardNum == 1)
                num = "ace";

            string currPath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\")) + "Resources\\";
            DirectoryInfo dirInfo = new DirectoryInfo(currPath);
            FileInfo[] files = dirInfo.GetFiles("*.png");

            foreach (FileInfo file in files)
            {
                fileName = file.Name.Split('_');
                if(fileName[0] == num && fileName[2][0] == cardSign)
                {
                    picture.ImageLocation = currPath + file.Name;
                }
            }
        }


        public void GenerateCards()
        {
            int i = 0; int width = 90;
            for (i = 0; i < 10; i++)
            {
                PictureBox pic = new PictureBox();
                pic.Image = Properties.Resources.card_back_red;
                pic.Location = new Point(13 + width * i, 325);
                pic.Name = "pic" + i;
                pic.Size = new Size(87, 115);
                pic.SizeMode = PictureBoxSizeMode.Zoom;
                Invoke((MethodInvoker)delegate { Controls.Add(pic); }); //place design and add the image
                pic.Click += delegate (object sender1, EventArgs e1)
                {
                    FoldAllCards();
                    string msg = CreateMessageToServer("1"); //get random card
                    PicturePlacer(msg, pic); //put the picture of the random card on the selected card
                    SendMessageToServer(msg); //send the card to the server
                    if(!(numOfSendedCards > 2))
                    {
                        numOfSendedCards++;
                        if (numOfSendedCards == 2) //check if you both send card
                        {
                            WinnerCalc(opponentMessage);  //compere who win
                            PicturePlacer(opponentMessage, opponent_card);
                            numOfSendedCards = 0;
                        }
                        else
                            myMessage = msg;
                    }
                };
                myCards[i] = pic; 
            }
        }


        public void ShowOpponentCard(string opponentMessage)
        {
            PicturePlacer(opponentMessage, opponent_card);
        }


        public void WinnerCalc(string opponent)
        {
            int yourNum = Int32.Parse(myScore.Text.Substring(1, 2));
            int opponentNum = Int32.Parse(opponent.Substring(1, 2));
            if (yourNum > opponentNum) //your player win
            {
                yourNum = Int32.Parse(myScore.Text);
                yourNum++;
                myScore.Text = yourNum.ToString();
                opponentNum = Int32.Parse(opponentScore.Text);
                opponentNum--;
                opponentScore.Text = opponentNum.ToString();
            }
            else if (yourNum < opponentNum) //your player lose
            {
                yourNum = Int32.Parse(myScore.Text);
                yourNum--;
                myScore.Text = yourNum.ToString();
                opponentNum = Int32.Parse(opponentScore.Text);
                opponentNum++;
                opponentScore.Text = opponentNum.ToString();
            }
        }


        public char GetRandomLetter()
        {
            Random rnd = new Random();
            int num = rnd.Next(4);
            switch (num)
            {
                case 0:
                    return 'H';
                case 1:
                    return 'C'; 
                case 2:
                    return 'D'; 
                case 3:
                    return 'S'; 
                default:
                    return 'N';
            }
        }


        public string GetRandomNum()
        {
            Random rnd = new Random();
            int number = rnd.Next(1, 13);
            string num = number.ToString();
            num = num.PadLeft(2, '0'); 
            return num;
        }

        private void front_interface_Load(object sender, EventArgs e)
        {

        }
    }
}
