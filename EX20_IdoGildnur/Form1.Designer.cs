﻿namespace EX20_IdoGildnur
{
    partial class front_interface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myScore = new System.Windows.Forms.Label();
            this.opponentScore = new System.Windows.Forms.Label();
            this.forfeitBtn = new System.Windows.Forms.Button();
            this.opponent_card = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.opponent_card)).BeginInit();
            this.SuspendLayout();
            // 
            // myScore
            // 
            this.myScore.AutoSize = true;
            this.myScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.myScore.Location = new System.Drawing.Point(21, 9);
            this.myScore.Name = "myScore";
            this.myScore.Size = new System.Drawing.Size(28, 16);
            this.myScore.TabIndex = 0;
            this.myScore.Text = ":00";
            // 
            // opponentScore
            // 
            this.opponentScore.AutoSize = true;
            this.opponentScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opponentScore.Location = new System.Drawing.Point(760, 12);
            this.opponentScore.Name = "opponentScore";
            this.opponentScore.Size = new System.Drawing.Size(28, 16);
            this.opponentScore.TabIndex = 1;
            this.opponentScore.Text = ":00";
            // 
            // forfeitBtn
            // 
            this.forfeitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forfeitBtn.Location = new System.Drawing.Point(400, 9);
            this.forfeitBtn.Name = "forfeitBtn";
            this.forfeitBtn.Size = new System.Drawing.Size(116, 30);
            this.forfeitBtn.TabIndex = 2;
            this.forfeitBtn.Text = "Forfeit";
            this.forfeitBtn.UseVisualStyleBackColor = true;
            this.forfeitBtn.Click += new System.EventHandler(this.forfeitBtn_Click);
            // 
            // opponent_card
            // 
            this.opponent_card.Image = global::EX20_IdoGildnur.Properties.Resources.card_back_blue;
            this.opponent_card.Location = new System.Drawing.Point(400, 57);
            this.opponent_card.Name = "opponent_card";
            this.opponent_card.Size = new System.Drawing.Size(116, 147);
            this.opponent_card.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.opponent_card.TabIndex = 3;
            this.opponent_card.TabStop = false;
            // 
            // front_interface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LawnGreen;
            this.ClientSize = new System.Drawing.Size(922, 489);
            this.Controls.Add(this.opponent_card);
            this.Controls.Add(this.forfeitBtn);
            this.Controls.Add(this.opponentScore);
            this.Controls.Add(this.myScore);
            this.Name = "front_interface";
            this.Text = "Cards Game";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.front_interface_FormClosed);
            this.Load += new System.EventHandler(this.front_interface_Load);
            ((System.ComponentModel.ISupportInitialize)(this.opponent_card)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label myScore;
        private System.Windows.Forms.Label opponentScore;
        private System.Windows.Forms.Button forfeitBtn;
        private System.Windows.Forms.PictureBox opponent_card;
    }
}

